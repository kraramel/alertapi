﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
/*using Involys.Poc.Api.Controllers.Commande.Model;
using Involys.Poc.Api.Data;
using Involys.Poc.Api.Services.Commandes;*/
using Involys.Poc.Api.Unit.Tests.Seeds;
using Involys.Poc.Api.Unit.Tests.Utils;
using Involys.Poc.Api.Utils.Logger;
using Microsoft.Extensions.Logging;
using Xunit;

namespace Involys.Poc.Api.Unit.Tests.Services
{
    public class CommandesServiceTests
    {
        /*private readonly DatabaseContext _context;
        private readonly ICommandeService _commandeService;

        private readonly Factory _factory;*/

        public CommandesServiceTests()
        {
            /*_context = new DatabaseContext(FakeServicesConfig.DbContextOptions("CommandesServicesTests"));
            _factory = new Factory(_context);
            var _mapper = FakeServicesConfig.AutoMapperService();
            var _logger = new LoggerAdapter<CommandeService>(new LoggerFactory());
            _commandeService = new CommandeService(_context, _mapper, _logger);

            new DatabaseSeeder(_context).CleanUp();
        }

        [Fact]
        public void GetCommandes()
        {
            // Arrange
            var commande = _factory.CreateCommandeWithLines("20", "objet 20", 1000);
            _context.SaveChanges();

            // Act
            var commandes = _commandeService.GetCommandes();

            // Assert
            var taskResult = Assert.IsType<Task<IEnumerable<CommandeResponse>>>(commandes);
            var commandesResult = Assert.IsAssignableFrom<IEnumerable<CommandeResponse>>(taskResult.Result);

            var lastCommade = commandesResult.Last();

            Assert.Equal(commande.Id, lastCommade.Id);
            Assert.Equal(commande.Numero, lastCommade.Numero);
            Assert.Equal(commande.Objet, lastCommade.Objet);
            Assert.Equal(commande.Montant, lastCommade.Montant);
            Assert.Null(commande.Statut);
        }*/
        }
    }
}