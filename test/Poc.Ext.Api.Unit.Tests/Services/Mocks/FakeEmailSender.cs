﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Involys.Poc.Api.Unit.Tests.Utils
{
    public class FakeEmailSender //: IEmailSender
    {
        public MailMessage SentMailMessage { get; set; }

        public List<MailMessage> SentMailMessages { get; set; } = new List<MailMessage>();

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var mailMessage = new MailMessage(
                "SenderEmail@mail.com",
                email, 
                subject, 
                htmlMessage)
            { IsBodyHtml = true };

            SentMailMessage = mailMessage;
            SentMailMessages.Add(mailMessage);
            return Task.CompletedTask;
        }
    }
}
