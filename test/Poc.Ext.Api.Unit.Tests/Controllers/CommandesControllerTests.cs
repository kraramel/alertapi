using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Xunit;
/*using Involys.Poc.Api.Data;
using Involys.Poc.Api.Controllers.Commande;
using Involys.Poc.Models;*/
using Involys.Poc.Api.Utils.Logger;
using Microsoft.Extensions.Logging;
using Involys.Poc.Api.Unit.Tests.Seeds;
using Involys.Poc.Api.Unit.Tests.Utils;

namespace Poc.Ext.Api.Unit.Tests
{
    public class CommandesControllerTests
    {
      /*  private readonly IMapper _mapper;
        private readonly DatabaseContext _context;
        private readonly CommandesController _controller;
        private readonly Factory _factory;
        private readonly CommandeService _commandeService;
        private readonly LoggerAdapter<CommandeService> _logger;

        private static readonly IEnumerable<CommandeDataModel> Commandes = new CommandeDataModel[]
        {
            new CommandeDataModel { Numero = "1", Objet = "commande 1",  Montant = 1000 },
            new CommandeDataModel { Numero = "2", Objet = "commande 2",  Montant = 2000 },
            new CommandeDataModel { Numero = "3", Objet = "commande 3",  Montant = 3000 },
            new CommandeDataModel { Numero = "4", Objet = "commande 4",  Montant = 4000 },
            new CommandeDataModel { Numero = "5", Objet = "commande 5",  Montant = 5000 },
        };

        public CommandesControllerTests()
        {
            _mapper = FakeServicesConfig.AutoMapperService();
            _context = new DatabaseContext(FakeServicesConfig.DbContextOptions("CommandesControllerTestDatabase"));
            _logger = new LoggerAdapter<CommandeService>(new LoggerFactory());
            _commandeService = new CommandeService(_context, _mapper, _logger);
            _controller = new CommandesController(_commandeService);

            _factory = new Factory(_context);

            var databaseSeeder = new DatabaseSeeder(_context);

            databaseSeeder.CleanUp();
            databaseSeeder.Insert(Commandes);
        }

        [Fact]
        public async Task GetCommandesListStatus200Async()
        {
            var result = await _controller.GetAllCommandes();

            var actionResult = Assert.IsType<ActionResult<IEnumerable<CommandeResponse>>>(result);
            var results = Assert.IsType<OkObjectResult>(actionResult.Result);            
            var commandesResult = Assert.IsAssignableFrom<IEnumerable<CommandeResponse>>(results.Value);
            
            var expectedList = _mapper.ProjectTo<CommandeResponse>(Commandes.AsQueryable())
                .OrderBy(e => e.Numero).ToList();
            var resultList = commandesResult.ToList();

            Assert.Equal(expectedList.Count, resultList.Count);
            for (int i = 0; i < expectedList.Count; i++)
            {
                Assert.Equal(expectedList[i].Id, resultList[i].Id);
            }
        }

        [Fact]
        public async Task GetCommandeStatus200()
        {
            int id = _context.Commandes.Last().Id;

            var result = await _controller.GetCommande(id);

            var okObjectResult = Assert.IsType<ActionResult<CommandeResponse>>(result);
            var Commande = Assert.IsAssignableFrom<CommandeResponse>(okObjectResult.Value);
            Assert.NotNull(Commande);
            Assert.Equal(id, Commande.Id);
        }

        [Fact]
        public async Task GetCommandeNotFound404()
        {
            var response = await _controller.GetCommande(-1);

            var notFoundReponse = Assert.IsType<ActionResult<CommandeResponse>>(response);
            Assert.Null(notFoundReponse.Value);
            var result = Assert.IsType<NotFoundResult>(notFoundReponse.Result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async Task PostCommande()
        {
            CreateCommandeQuery query = new CreateCommandeQuery { Numero = "123", Objet = "objet", Montant = 10000  };

            var response = await _controller.PostCommande(query);

            *//* Check it is inserted in the Context *//*
            var lastCommande = _context.Commandes.OrderBy(c => c.Id).Last();
            Assert.Equal(query.Numero, lastCommande.Numero);
            Assert.Equal(query.Objet, lastCommande.Objet);
            Assert.Equal(query.Montant, lastCommande.Montant);

            *//* Check it returns the object with 201 status *//*
            var actionResult = Assert.IsType<ActionResult<CommandeResponse>>(response);
            var result = Assert.IsType<CreatedAtActionResult>(actionResult.Result);
            var value = Assert.IsType<CommandeResponse>(result.Value);

            Assert.Equal(query.Numero, value.Numero);
            Assert.Equal(query.Objet, value.Objet);
            Assert.Equal(query.Montant, value.Montant);
        }

        [Fact(Skip = "Validation problem Null Reference exception")]
        // [Fact]
        public async Task PostCommandeDuplicate_returnsBadRequest()
        {
            // Arrange 
            var otherCommande = _factory.CreateCommande("10", "objet 10", 10000);
            await _context.SaveChangesAsync();

            // Act
            var query = new CreateCommandeQuery { Numero = otherCommande.Numero, Objet=otherCommande.Objet, Montant=otherCommande.Montant };
            var response = await _controller.PostCommande(query);

            // Assert
            var actionResult = Assert.IsType<ActionResult<CommandeResponse>>(response);
            Assert.IsType<BadRequestObjectResult>(actionResult.Result);
        }

        [Fact]
        public async Task UpdateCommande()
        {
            var Commande = _context.Commandes.Last();
            var query = new UpdateCommandeQuery { Numero = "10 updated", Objet = "objet 10 updates", Montant = 15000};

            var response = await _controller.PutCommande(Commande.Id, query);

            var updatedCommande = _context.Commandes.FirstOrDefault(e => e.Id == Commande.Id);

            Assert.Equal(query.Numero, updatedCommande.Numero);
            Assert.Equal(query.Objet, updatedCommande.Objet);
            Assert.Equal(query.Montant, updatedCommande.Montant);

            *//* Check it returns no content - 204 status *//*
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async Task UpdateOnlyNumeroField()
        {
            var commande = _context.Commandes.Last();
            var query = new UpdateCommandeQuery { Numero = "10 updated" };

            await _controller.PutCommande(commande.Id, query);

            var updatedCommande = _context.Commandes.FirstOrDefault(e => e.Id == commande.Id);

            Assert.Null(commande.Statut);
            Assert.Equal(query.Numero, updatedCommande.Numero); // Updated
        }

        [Fact]
        public async Task UpdateInexistantCommandeReturns404()
        {
            var query = new UpdateCommandeQuery { Numero = "10 updated", Objet = "objet 10 updates", Montant = 15000 };

            var response = await _controller.PutCommande(-1, query);

            // Check directly if it's a NotFoundResult. B/c Put returns an IActionResult
            var result = Assert.IsType<NotFoundResult>(response);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact(Skip = "Validation problem Null Reference exception")]
        public async Task PutCommandeDuplicateNumero_returnsBadRequest()
        {
            var commande = _factory.CreateCommande("Commande 1", "Objet 1", 1);
            var otherCommande = _factory.CreateCommande("Commande 2", "Objet 2", 2);
            await _context.SaveChangesAsync();
            var query = new UpdateCommandeQuery { Numero = otherCommande.Numero };

            var response = await _controller.PutCommande(commande.Id, query);

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task Put_NotDuplicate_IfSameCommandeIsModified()
        {
            var Commande = _factory.CreateCommande("Commande 1", "Objet 1", 1);
            await _context.SaveChangesAsync();
            var query = new UpdateCommandeQuery { Numero = Commande.Numero };

            var response = await _controller.PutCommande(Commande.Id, query);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async Task DeleteCommande()
        {
            var id = _context.Commandes.Add(new CommandeDataModel
            {
                Numero = "11",
                Objet = "commande 11",
                Montant = 11000
            }).Entity.Id;

            var response = await _controller.DeleteCommande(id);

            var actionResult = Assert.IsType<ActionResult<CommandeResponse>>(response);
            var Commande = Assert.IsAssignableFrom<CommandeResponse>(actionResult.Value);
            Assert.NotNull(Commande);
            Assert.Equal(id, Commande.Id);
            Assert.Equal(0, _context.Commandes.Count(e => e.Id == id)); // Should not Find the Object in the store
        }
        
        [Fact]
        public async Task DeleteInexistantCommandeReturns404()
        {
            int id = -1;

            var response = await _controller.DeleteCommande(id);

            // This step of parsing is necessary bc the method returns a concrete ActionResult<T>, an not a IActionResult
            var notFoundReponse = Assert.IsType<ActionResult<CommandeResponse>>(response);
            var result = Assert.IsType<NotFoundResult>(notFoundReponse.Result);
            Assert.Equal(404, result.StatusCode);
        }*/
    }
}