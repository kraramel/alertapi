﻿using System.Collections.Generic;
using Involys.Poc.Api.Data;
using Involys.Poc.Models;

namespace Involys.Poc.Api.Integration.Tests.Utils
{
    public static class DatabaseSeeder
    {
        public static void Seed(DatabaseContext dbContext)
        {
                dbContext.Commandes.AddRange(CommandesSeeds);
                dbContext.SaveChanges();

        }


        private static readonly IEnumerable<CommandeDataModel> CommandesSeeds = new CommandeDataModel[]
        {
            new CommandeDataModel { Numero = "1", Objet = "commande 1",  Montant = 1000 },
            new CommandeDataModel { Numero = "2", Objet = "commande 2",  Montant = 2000 },
            new CommandeDataModel { Numero = "3", Objet = "commande 3",  Montant = 3000 },
            new CommandeDataModel { Numero = "4", Objet = "commande 4",  Montant = 4000 },
            new CommandeDataModel { Numero = "5", Objet = "commande 5",  Montant = 5000 },
        };
       
    }
}