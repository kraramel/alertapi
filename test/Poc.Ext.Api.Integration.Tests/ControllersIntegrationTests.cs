﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Web;
using Involys.Poc.Api.Integration.Tests.Utils;

namespace Poc.Ext.Api.Integration.Tests
{
    public class ControllersIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public ControllersIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Theory]
        [InlineData("/api/v1/Commandes")]
        [InlineData("/api/v1/Commandes/all")]
        [InlineData("/api/v1/Commandes/1")]        
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(url);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            Assert.Equal("application/json; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
        }

        [Theory]
        [InlineData("/api/v1/Commandes/0")]        
        public async Task GetById_ReturnsNotFoundAsync(string url)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(url);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
            Assert.Equal("application/json; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
        }        
    }
}