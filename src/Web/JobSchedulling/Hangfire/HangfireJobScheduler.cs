﻿using Hangfire;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Web.Services.ScalingService;
using Web.Utils.Jobs;

namespace Web.JobSchedulling.Hangfire
{
    public class HangfireJobScheduler
    {
        private readonly DatabaseContext _context;

        public HangfireJobScheduler(DatabaseContext context)
        {
            _context = context;
        }

        /// <summary>
        /// The job that will be executed every minute to check changes of the config
        /// and launch the job or delete it
        /// </summary>
        /// <returns>A job given</returns>
        public async Task SchedulerRucurringJobs()
        {
            List<AlertConfigDataModel> jobsToDo = await _context.AlertConfigs.OrderBy(e => e.Id).ToListAsync();

            foreach (var job in jobsToDo)
            {
                if (job.Frequence != Constants.Constants.AtGivenDateHour)
                { 
                    //Execute with the frequency
                    RecurringJob.AddOrUpdate<IScalingService>(job.Id.ToString(), jobR => jobR.AddNotification(job.Id),
                      getExpressionFromFrequency(job), TimeZoneInfo.Local);
                }
                
            }

            //Deleting jobs if they are recursiv and expiring date is now
            foreach (var job in jobsToDo)
            {
                if (DateTime.Compare(job.DateOfExpiration, DateTime.Now) < 0)
                {
                    //Desactivating the job if date of expire is now
                    /*job.Activated = false;*/
                    _context.SaveChanges();
                    /*Console.WriteLine("=========> Deleting job with id:{0} after comparation retruned {1}", job.Id.ToString(), DateTime.Compare(job.JobSchedul.DateOfExpiration, DateTime.Now));*/
                    //Deleting from hangfire
                    /*RecurringJob.RemoveIfExists(job.Id.ToString());*/
                }
            }
        }


        /// <summary>
        /// this method is for getting the cron expression from the frequency of save
        /// </summary>
        /// <param name="job"></param>
        /// <returns>Cron expression that can be used inside Hangfire</returns>
        public string getExpressionFromFrequency(AlertConfigDataModel job)
        {
            var frequency = job.Frequence;
            switch (frequency)
            {
                case Constants.Constants.EveryMinute:
                    CronJobExpressions cronExMin = new CronJobExpressions();
                    return cronExMin.EveryMinutes(job.ExecuteEveryMinutes);
                case Constants.Constants.EveryHour:
                    CronJobExpressions cronExHour = new CronJobExpressions();
                    return cronExHour.EveryHoursAtMinutes(job.ExecuteEveryHours, job.EveryHoursAtMinutes);
                case Constants.Constants.EveryDay:
                    CronJobExpressions cronEx = new CronJobExpressions();
                    DateTime timeOfExecute = DateTime.ParseExact
                        (job.HourOfExecuute, "HH:mm", CultureInfo.InvariantCulture);

                    return cronEx.DailyAtHourAndMinute(timeOfExecute.Hour, timeOfExecute.Minute);
                case Constants.Constants.DaysOfWeek:
                    CronJobExpressions cronExDaysOfweek = new CronJobExpressions();
                    DateTime timeOfExecuteDayOfweek = DateTime.ParseExact
                        (job.HourOfExecuute, "HH:mm", CultureInfo.InvariantCulture);
                    List<DayOfWeek> daysOfWeek = new List<DayOfWeek>();
                    if (job.SchedulMonday) { daysOfWeek.Add(DayOfWeek.Monday); }
                    if (job.SchedulTuesday) { daysOfWeek.Add(DayOfWeek.Tuesday); }
                    if (job.SchedulWednesday) { daysOfWeek.Add(DayOfWeek.Wednesday); }
                    if (job.SchedulThursday) { daysOfWeek.Add(DayOfWeek.Thursday); }
                    if (job.SchedulFriday) { daysOfWeek.Add(DayOfWeek.Friday); }
                    if (job.SchedulSaturday) { daysOfWeek.Add(DayOfWeek.Saturday); }
                    if (job.SchedulSunday) { daysOfWeek.Add(DayOfWeek.Sunday); }

                    return cronExDaysOfweek.AtHourAndMinuteOnGivenDaysOfWeek
                        (timeOfExecuteDayOfweek.Hour, timeOfExecuteDayOfweek.Minute, daysOfWeek);
                /*        case Constants.EveryWeek:
                          CronJobExpressions cronExDayOfweek = new CronJobExpressions();
                          DateTime timeOfExecuteDayOfweeks = DateTime.ParseExact
                              (job.JobSchedul.HourOfExecuute, "HH:mm", CultureInfo.InvariantCulture);
                          DayOfWeek dayOfWeek = new DayOfWeek();
                          if (job.JobSchedul.SchedulMonday) { dayOfWeek = DayOfWeek.Monday; }
                          if (job.JobSchedul.SchedulTuesday) { dayOfWeek = DayOfWeek.Tuesday; }
                          if (job.JobSchedul.SchedulWednesday) { dayOfWeek = DayOfWeek.Wednesday; }
                          if (job.JobSchedul.SchedulThursday) { dayOfWeek = DayOfWeek.Thursday; }
                          if (job.JobSchedul.SchedulFriday) { dayOfWeek = DayOfWeek.Friday; }
                          if (job.JobSchedul.SchedulSaturday) { dayOfWeek = DayOfWeek.Saturday; }
                          if (job.JobSchedul.SchedulSunday) { dayOfWeek = DayOfWeek.Sunday; }

                          return cronExDayOfweek.WeeklyOnDayAndHourAndMinute(dayOfWeek, timeOfExecuteDayOfweeks.Hour, timeOfExecuteDayOfweeks.Minute);*/
                case Constants.Constants.EveryMonth:
                    CronJobExpressions cronExDayOfMoth = new CronJobExpressions();
                    DateTime timeOfExecuteDayOfMoth = DateTime.ParseExact
                        (job.HourOfExecuute, "HH:mm", CultureInfo.InvariantCulture);
                    int dayOfMoth = job.DayOfMonth;

                    if (dayOfMoth == 31)
                    {
                        dayOfMoth = LastDayOfMonth(timeOfExecuteDayOfMoth);
                    }

                    return cronExDayOfMoth.MonthlyOnDayAndHourAndMinute(dayOfMoth, timeOfExecuteDayOfMoth.Hour, timeOfExecuteDayOfMoth.Minute);
            }
            return "";
        }

        /// <summary>
        /// Getting the last day of a month
        /// </summary>
        /// <param name="timeOfExecuteDayOfMoth"></param>
        /// <returns>Returns the last day if every month if 31th is given</returns>
        private int LastDayOfMonth(DateTime timeOfExecuteDayOfMoth)
        {
            DateTime result = new DateTime(timeOfExecuteDayOfMoth.Year, timeOfExecuteDayOfMoth.Month, 1);
            return result.AddMonths(1).AddDays(-1).Day;
        }
    }
}
