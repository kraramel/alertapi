﻿
using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Scaling.Model;
using Web.Services.ScalingService;

namespace Web.Controllers.Scaling
{
    [ApiController]
    [Route("api/[controller]")]
    public class ScalingController : ControllerBase
    {
        private readonly IScalingService _scalingService;
        private readonly DatabaseContext _context;
        private readonly IMapper _mapper;
        public ScalingController(IMapper mapper, IScalingService scalingService, DatabaseContext context)
        {
            _scalingService = scalingService;
            _context = context;
            _mapper = mapper;
        }

        public object GetPropertyValue(object obj, string propertyName)
        {
            var objType = obj.GetType();
            var prop = objType.GetProperty(propertyName);

            return prop.GetValue(obj, null);
        }


        // Return column names of the table
        [HttpGet("GetColumns")]
        public ActionResult GetColumnNames()
        {

            string[] names = typeof(ScalingDataModel).GetProperties()
                         .Select(property => property.Name)
                         .ToArray();
            return Ok(names);
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _scalingService.GetAllScalings());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            return Ok(await _scalingService.GetScalingById(id));
        }

        [HttpGet("alertconfig/{id}")]
        public async Task<IActionResult> GetScalingByAlertConfigId(int id)
        {
            return Ok(await _scalingService.GetScalingByAlertConfigId(id));
        }

        [HttpGet("user/{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            return Ok(await _scalingService.GetUsersByScalingId(id));
        }

        [HttpPost]
        public async Task<IActionResult> AddAlertConfig(AddScalingDto newScaling) //create 
        {
            return Ok(await _scalingService.AddScaling(newScaling));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateScaling(UpdateScalingQuery updatedScaling)
        {
            ScalingResponse2 response = await _scalingService.UpdateScaling(updatedScaling);
            if (response == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            List<ScalingResponse2> response = await _scalingService.DeleteScaling(id);
            if (response == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        /*[HttpGet("addnotif/{id}")]
        public async Task<IActionResult> AddNottification(int id)
        {
            return Ok(await _scalingService.AddNotif(id));
        }*/

        [HttpGet("addnotiff")]
        public async Task<IActionResult> Test()
        {
            var commande = _context.Commandes
                     .Where(c => c.Quantite > 50)
                     .ToList();
            /*if (commande != null)
            {*/
            ScalingDataModel dbScaling = await _context.Scalings.Include(i => i.AlertConfig)
                                                   .Where(c => c.AlertConfig.Id == 2)
                                                   .Include(x => x.ScalingUsers).ThenInclude(x => x.User)
                                                   .FirstOrDefaultAsync();

           

            AlertNotificationDataModel newAlertNotification = new AlertNotificationDataModel
            {
                Titre = "TesSTEUR 96969696969",
                AlertConfigId = 2

            };

            await _context.AlertNotifications.AddAsync(newAlertNotification);

            await _context.SaveChangesAsync();



            var address = GetPropertyValue(dbScaling, "Nom");

            return Ok(address);

        }


    }
}
