﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.Scaling.Model
{
    public class AddScalingDto
    {
        public string Nom { get; set; }
        public string Conditions { get; set; }
    }
}
