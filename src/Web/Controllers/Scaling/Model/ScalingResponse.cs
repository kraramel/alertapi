﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Group;
using Web.Controllers.Role.Models;
using Web.Controllers.User.Model;

namespace Web.Controllers.Scaling.Model
{
    public class ScalingResponse
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Conditions { get; set; }
        public List<UserResponse> Users { get; set; }
        public List<GroupResponse> Groups { get; set; }
        public List<RoleResponse> Roles { get; set; }
    }
}
