﻿using Infrastructure.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.AlertConfig.Model;
using Web.Services.AlertConfigService;

namespace Web.Controllers.AlertConfig
{
    [ApiController]
    [Route("api/[controller]")]
    public class AlertConfigController : ControllerBase
    {
        private readonly IAlertConfigService _alertConfigService;

        public AlertConfigController(IAlertConfigService alertConfigService)
        {
            _alertConfigService = alertConfigService;
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _alertConfigService.GetAllAlertConfigs());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            return Ok(await _alertConfigService.GetAlertConfigById(id));
        }

        [HttpPost]
        public async Task<IActionResult> AddAlertConfig(CreateAlertConfigQuery newAlertConfig) //create 
        {
            return Ok(await _alertConfigService.AddAlertConfig(newAlertConfig));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAlertConfig(int id,  UpdateAlertConfigQuery updatedAlertConfig)
        {
            AlertConfigResponse response = await _alertConfigService.UpdateAlertConfig(updatedAlertConfig, id);
            if (response == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            List<AlertConfigResponse> response = await _alertConfigService.DeleteAlertConfig(id);
            if (response == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpGet("GetColumns")]
        public ActionResult GetColumnNames()
        {

            string[] names = typeof(AlertConfigDataModel).GetProperties()
                         .Select(property => property.Name)
                         .ToArray();
            return Ok(names);
        }

    }
}
