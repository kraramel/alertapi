﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.AlertNotification.Model;
using Web.Controllers.Scaling.Model;

namespace Web.Controllers.AlertConfig.Model
{
    public class AlertConfigResponse
    {
        public int Id { get; set; }
        public string Designation { get; set; }
        public string NiveauCriticite { get; set; }
        public string Etat { get; set; } // Une alertNotification a un etat et non pas une alert config
        public string TextAlert { get; set; }
        public string Module { get; set; }
        public string Frequence { get; set; }
        public string SourceDonnee { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }

        public List<UpdateAlertNotificationQuery> AlertNotifications { get; set; }
        public List<UpdateScalingQuery> Scalings { get; set; }


    }
}
