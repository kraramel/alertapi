﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.ScalingGroup.Model
{
    public class CreateScalingGroupQuery
    {
        public int ScalingId { get; set; }
        public int GroupId { get; set; }
    }
}
