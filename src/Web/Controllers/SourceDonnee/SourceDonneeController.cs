﻿using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Services.SourceDoneeService;
using Web.Controllers.SourceDonnee.Model;

namespace Web.Controllers.SourceDonnee
{
    [ApiController]
    [Route("api/[controller]")]
    public class SourceDonneeController : ControllerBase
    {
        private readonly ISourceDonneeService _sourceDonneeService;

        public SourceDonneeController(ISourceDonneeService sourceDonneeService)
        {
            _sourceDonneeService = sourceDonneeService;
        }
        [HttpPost]
        public async Task<IActionResult> AddSourceDonnee(CreateSourceDonneeQuery newSourceDonnee) //create 
        {
            return Ok(await _sourceDonneeService.AddSourceDonnee(newSourceDonnee));
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _sourceDonneeService.GetAllSourceDonnees());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            return Ok(await _sourceDonneeService.GetSourceDonneeById(id));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateSourceDonnee(int id, UpdateSourceDonneeQuery updatedSourceDonnee)
        {
            SourceDonneeResponse response = await _sourceDonneeService.UpdateSourceDonnee(updatedSourceDonnee, id);
            if (response == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            List<SourceDonneeResponse> response = await _sourceDonneeService.DeleteSourceDonnee(id);
            if (response == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }


    }
}
