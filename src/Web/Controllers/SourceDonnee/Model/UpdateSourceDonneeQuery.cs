﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.SourceDonnee.Model
{
    public class UpdateSourceDonneeQuery
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Table { get; set; }
        public string Requete { get; set; }
    }
}
