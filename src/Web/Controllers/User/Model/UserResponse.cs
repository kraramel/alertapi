﻿using Infrastructure.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.User.Model
{
    public class UserResponse
    {
        public string Id { get; set; }
        [StringLength(256), Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(256), Required]
        public string UserName { get; set; }
        [StringLength(256), Required]
        public string FirstName { get; set; }
        [StringLength(256), Required]
        public string LastName { get; set; }
        [StringLength(256), Required]
        public string PhoneNumber { get; set; }
        public string Profil { get; set; }
        [Required]
        public bool IsActive { get; set; }
        public DateTime RegistrationDate { get; set; }

        public DomainDataModel Domain { get; set; }

        public bool EmailConfirmed { get; set; }

        public string ImgPath { get; set; }
    }
}
