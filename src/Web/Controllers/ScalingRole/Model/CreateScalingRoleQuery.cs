﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.ScalingRole.Model
{
    public class CreateScalingRoleQuery
    {
        public int ScalingId { get; set; }
        public int RoleId { get; set; }
    }
}
