﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.ScalingUser.Model;
using Web.Services.ScalingUserService;

namespace Web.Controllers.ScalingUser
{
    [ApiController]
    [Route("api/[controller]")]
    public class ScalingUserController : ControllerBase
    {
        private readonly IScalingUserService _scalingUserService;
        public ScalingUserController(IScalingUserService scalingUserService)
        {
            _scalingUserService = scalingUserService;
        }

        [HttpPost]
        public async Task<IActionResult> AddScalingUser(CreateScalingUserQuery newScalingUser)
        {
            return Ok(await _scalingUserService.AddScalingUser(newScalingUser));
        }


    }
}
