﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.ScalingUser.Model
{
    public class CreateScalingUserQuery
    {
        public int ScalingId { get; set; }
        public int UserId { get; set; }
    }
}
