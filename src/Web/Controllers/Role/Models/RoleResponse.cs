﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.Role.Models
{
    public class RoleResponse
    {
        public string Name { get; set; }
    }
}
