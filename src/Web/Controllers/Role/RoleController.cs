﻿using Common.Email;
using Infrastructure.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Services.RoleService;

namespace Web.Controllers.Role
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly IEmailSender _emailSender;
        public RoleController(IRoleService roleService, IEmailSender emailSender)
        {
            _roleService = roleService;
            _emailSender = emailSender;
        }

        [HttpGet("GetColumns")]
        public ActionResult GetColumnNames()
        {

            string[] names = typeof(RoleDataModel).GetProperties()
                         .Select(property => property.Name)
                         .ToArray();
            return Ok(names);
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _roleService.GetAllRoles());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            return Ok(await _roleService.GetRoleById(id));
        }

        [HttpGet("sendmail")]
        public async Task<IActionResult> GetMail()
        {
            var message = new Message(new string[] { "koolage.1996@gmail.com" }, "Test email async", "This is the content from our async email.");
            await _emailSender.SendEmailAsync(message);
            return Ok();
        }
    }
}
