﻿
using Infrastructure.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.AlertNotification.Model;
using Web.Services.AlertNotificationService;

namespace Web.Controllers.AlertNotification
{
    [ApiController]
    [Route("api/[controller]")]
    public class AlertNotificationController : ControllerBase
    {
        private readonly IAlertNotificationService _alertNotificationService;

        public AlertNotificationController(IAlertNotificationService alertNotificationService)
        {
            _alertNotificationService = alertNotificationService;
        }

        [HttpGet("GetColumns")]
        public ActionResult GetColumnNames()
        {

            string[] names = typeof(AlertNotificationDataModel).GetProperties()
                         .Select(property => property.Name)
                         .ToArray();
            return Ok(names);
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _alertNotificationService.GetAllAlertNotifications());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            return Ok(await _alertNotificationService.GetAlertNotificationById(id));
        }

        [HttpPost]
        public async Task<IActionResult> AddAlertNotification(CreateAlertNotificationQuery newAlertNotification) //create 
        {
            return Ok(await _alertNotificationService.AddAlertNotification(newAlertNotification));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAlertNotification(UpdateAlertNotificationQuery updatedAlertNotification)
        {
            AlertNotificationResponse2 response = await _alertNotificationService.UpdateAlertNotification(updatedAlertNotification);
            if (response == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            List<AlertNotificationResponse2> response = await _alertNotificationService.DeleteAlertNotification(id);
            if (response == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }



    }
}
