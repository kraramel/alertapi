﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.AlertNotification.Model
{
    public class CreateAlertNotificationQuery
    {
        public string Titre { get; set; }
        public string Description { get; set; }
        public DateTime DateConsultation { get; set; }
        public DateTime DateEnvoi { get; set; }
/*        public int AlertConfigId { get; set; }
*/
    }
}
