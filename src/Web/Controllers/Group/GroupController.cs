﻿using Infrastructure.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Services.GroupService;

namespace Web.Controllers.Group
{
    [ApiController]
    [Route("api/[controller]")]
    public class GroupController : ControllerBase
    {
        private readonly IGroupService _groupService;

        public GroupController(IGroupService groupService)
        {
            _groupService = groupService;
        }

        [HttpGet("GetColumns")]
        public ActionResult GetColumnNames()
        {

            string[] names = typeof(GroupDataModel).GetProperties()
                         .Select(property => property.Name)
                         .ToArray();
            return Ok(names);
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _groupService.GetAllGroups());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            return Ok(await _groupService.GetGroupById(id));
        }
    }
}
