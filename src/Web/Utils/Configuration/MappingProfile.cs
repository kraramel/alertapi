﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;
using Infrastructure.Data.Models;
using Web.Controllers.AlertConfig.Model;
using Web.Controllers.AlertNotification.Model;
using Web.Controllers.Group;
using Web.Controllers.Role.Models;
using Web.Controllers.Scaling.Model;
using Web.Controllers.SourceDonnee.Model;
using Web.Controllers.User.Model;

namespace Web.Utils.Configuration
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

                
            CreateMap<AlertConfigDataModel, AlertConfigResponse>();
            

            CreateMap<AlertConfigDataModel, CreateAlertConfigQuery > ();
            CreateMap<AlertNotificationDataModel, AlertNotificationResponse>()
                 .ForMember(dto => dto.Users, c => c.MapFrom(c => c.AlertNotificationUsers.Select(cs => cs.User)));

          /*  ------------------------------------!!!!!!-------------------*/
            CreateMap<UserDataModel, UserResponse>();
/*            CreateMap<UserResponse, UserDataModel>();
*/            CreateMap<GroupDataModel, GroupResponse>();
            CreateMap<RoleDataModel, RoleResponse>();
            CreateMap<SourceDonneeDataModel, SourceDonneeResponse>();

            CreateMap<AlertNotificationDataModel, AlertNotificationResponse2>()
                .ForMember(dto => dto.Users, c => c.MapFrom(c => c.AlertNotificationUsers.Select(cs => cs.User))); ;
            CreateMap<AlertNotificationResponse2, AlertNotificationDataModel>();
            

            CreateMap<UpdateAlertNotificationQuery, AlertNotificationDataModel>();
            CreateMap<AlertNotificationDataModel, UpdateAlertNotificationQuery>();

            CreateMap<CreateAlertNotificationQuery, AlertNotificationDataModel>();
            CreateMap<CreateAlertConfigQuery, AlertConfigDataModel>();
            CreateMap<CreateSourceDonneeQuery, SourceDonneeDataModel>();

            CreateMap<ScalingDataModel, ScalingResponse>()
                 .ForMember(dto => dto.Users, c => c.MapFrom(c => c.ScalingUsers.Select(cs => cs.User)))
                 .ForMember(dto => dto.Roles, c => c.MapFrom(c => c.ScalingRoles.Select(cs => cs.Role)))
                 .ForMember(dto => dto.Groups, c => c.MapFrom(c => c.ScalingGroups.Select(cs => cs.Group)));


            CreateMap<ScalingDataModel, ScalingResponse2>()
                .ForMember(dto => dto.Users, c => c.MapFrom(c => c.ScalingUsers.Select(cs => cs.User)))
                .ForMember(dto => dto.Roles, c => c.MapFrom(c => c.ScalingRoles.Select(cs => cs.Role)))
                .ForMember(dto => dto.Groups, c => c.MapFrom(c => c.ScalingGroups.Select(cs => cs.Group)));
            CreateMap<ScalingResponse2, ScalingDataModel>();

            CreateMap<UpdateSourceDonneeQuery, SourceDonneeDataModel>();

            CreateMap<UpdateScalingQuery, ScalingDataModel>();
            CreateMap<ScalingDataModel, UpdateScalingQuery>();

            CreateMap<AddScalingDto, ScalingDataModel>();



        }
    }
}
