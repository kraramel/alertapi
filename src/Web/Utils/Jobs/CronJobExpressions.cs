﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Utils.Jobs
{
    //This class is for creating Cron Expressions that we will be using in our Job Schedulling
    public class CronJobExpressions
    {
        //This function is for creating an expression for launching job every n minute
        public string EveryMinutes(int minutes)
        {
            return $"0 */{minutes} * ? * *";
        }

        //This function is for creating an expression for launching job every n hours at n minutes
        public string EveryHoursAtMinutes(int hours, int minutes)
        {
            return $"0 {minutes} */{hours} ? * *";
        }

        //This function is for creating an expression for a given Hour and Minute on every day
        public string DailyAtHourAndMinute(int hour, int minute)
        {
            return $"0 {minute} {hour} ? * *";
        }

        //This function is for creating an expression for a given Hour and Minute on a given days of every week
        public string AtHourAndMinuteOnGivenDaysOfWeek(int hour, int minute, List<DayOfWeek> daysOfWeek)
        {
            if (daysOfWeek == null || daysOfWeek.Count == 0)
            {
                throw new ArgumentException("You must specify at least one day of week.");
            }

            string cronExpression = $"0 {minute} {hour} ? * {(int)daysOfWeek[0] + 1}";
            for (int i = 1; i < daysOfWeek.Count; i++)
            {
                cronExpression = cronExpression + "," + ((int)daysOfWeek[i] + 1);
            }

            return cronExpression;
        }

        //This function is for creating an expression for a given Hour and Minute on one given day every week
        public string WeeklyOnDayAndHourAndMinute(DayOfWeek dayOfWeek, int hour, int minute)
        {
            return $"0 {minute} {hour} ? * {(int)dayOfWeek + 1}";
        }

        //This function is for creating an expression for a given Hour and Minute on a given day every month
        public string MonthlyOnDayAndHourAndMinute(int dayOfMonth, int hour, int minute)
        {
            return $"0 {minute} {hour} {dayOfMonth} * ?";
        }
    }
}
