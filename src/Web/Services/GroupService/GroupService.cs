﻿using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Group;

namespace Web.Services.GroupService
{
    public class GroupService : IGroupService
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;

        public GroupService(IMapper mapper, DatabaseContext context)
        {
            _context = context;
            _mapper = mapper;
        }



        public async Task<List<GroupResponse>> GetAllGroups()
        {
            List<GroupResponse> serviceResponse = new List<GroupResponse>();

            //Get the Characters from the database
            List<GroupDataModel> dbScalings = await _context.Groups.ToListAsync();

            serviceResponse = dbScalings.Select(c => _mapper.Map<GroupResponse>(c)).ToList();
            return serviceResponse;
        }


        public async Task<GroupResponse> GetGroupById(int id)
        {
            GroupResponse serviceResponse = new GroupResponse();

            GroupDataModel dbScaling = await _context.Groups.FindAsync(id);
            serviceResponse = _mapper.Map<GroupResponse>(dbScaling);

            return serviceResponse;
        }
    }
}
