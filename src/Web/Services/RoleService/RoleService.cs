﻿using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Role.Models;

namespace Web.Services.RoleService
{
    public class RoleService : IRoleService
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;

        public RoleService(IMapper mapper, DatabaseContext context)
        {
            _context = context;
            _mapper = mapper;
        }



        public async Task<List<RoleResponse>> GetAllRoles()
        {
            List<RoleResponse> serviceResponse = new List<RoleResponse>();

            //Get the Characters from the database
            List<RoleDataModel> dbScalings = await _context.Roles.ToListAsync();

            serviceResponse = dbScalings.Select(c => _mapper.Map<RoleResponse>(c)).ToList();
            return serviceResponse;
        }


        public async Task<RoleResponse> GetRoleById(int id)
        {
            RoleResponse serviceResponse = new RoleResponse();

            RoleDataModel dbScaling = await _context.Roles.FindAsync(id);
            serviceResponse = _mapper.Map<RoleResponse>(dbScaling);

            return serviceResponse;
        }
    }

    }
