﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.AlertNotification.Model;

namespace Web.Services.AlertNotificationService
{
    public interface IAlertNotificationService
    {
        Task<List<AlertNotificationResponse2>> GetAllAlertNotifications();
        Task<AlertNotificationResponse2> GetAlertNotificationById(int id);
        Task<List<AlertNotificationResponse>> AddAlertNotification(CreateAlertNotificationQuery newAlertNotification);
        Task<AlertNotificationResponse2> UpdateAlertNotification(UpdateAlertNotificationQuery updatedAlertNotification);
        Task<List<AlertNotificationResponse2>> DeleteAlertNotification(int id);
    }
}
