﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.SourceDonnee.Model;

namespace Web.Services.SourceDoneeService
{
    public interface ISourceDonneeService
    {
        Task<List<SourceDonneeResponse>> GetAllSourceDonnees();
        Task<SourceDonneeResponse> GetSourceDonneeById(int id);
        Task<List<SourceDonneeResponse>> AddSourceDonnee(CreateSourceDonneeQuery newSourceDonnee);
        Task<SourceDonneeResponse> UpdateSourceDonnee(UpdateSourceDonneeQuery updatedSourceDonnee, int i);
        Task<List<SourceDonneeResponse>> DeleteSourceDonnee(int id);

    }
}
