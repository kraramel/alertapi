﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Scaling.Model;
using Web.Controllers.User.Model;

namespace Web.Services.ScalingService
{
    public interface IScalingService
    {
        Task<List<ScalingResponse2>> GetAllScalings();
        Task<ScalingResponse2> GetScalingById(int id);
        Task<List<ScalingResponse>> AddScaling(AddScalingDto newScaling);
        Task<ScalingResponse2> UpdateScaling(UpdateScalingQuery updatedScaling);
        Task<List<ScalingResponse2>> DeleteScaling(int id);
        /*Task<List<UserResponse>> AddNotif(int id);*/
        Task<List<UserResponse>> AddNotification(int id);
        Task<List<UserResponse>> GetUsersByScalingId(int id);
        Task<List<ScalingResponse2>> GetScalingByAlertConfigId(int id);


    }
}
