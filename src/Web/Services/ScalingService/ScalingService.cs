﻿
using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Scaling.Model;
using Web.Controllers.User.Model;

namespace Web.Services.ScalingService
{
    public class ScalingService : IScalingService
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;

        public ScalingService(IMapper mapper, DatabaseContext context)
        {
            _context = context;
            _mapper = mapper;
        }
        public object GetPropertyValue(object obj, string propertyName)
        {
            var objType = obj.GetType();
            var prop = objType.GetProperty(propertyName);

            return prop.GetValue(obj, null);
        }

        public async Task<List<ScalingResponse>> AddScaling(AddScalingDto newScaling)
        {
            List<ScalingResponse> serviceResponse = new List<ScalingResponse>();

            _context.Add(_mapper.Map<ScalingDataModel>(newScaling));
            await _context.SaveChangesAsync();
            List<ScalingDataModel> dbScalings = await _context.Scalings.Include(i => i.AlertConfig)
                                                              .Include(x => x.ScalingUsers).ThenInclude(x => x.User)
                                                              .Include(x => x.ScalingGroups)
                                                              .ThenInclude(x => x.Group).ToListAsync();

            serviceResponse = dbScalings.Select(c => _mapper.Map<ScalingResponse>(c)).ToList();
            return serviceResponse;
        }


        public async Task<List<ScalingResponse2>> GetAllScalings()
        {
            List<ScalingResponse2> serviceResponse = new List<ScalingResponse2>();

            //Get the Characters from the database
            List<ScalingDataModel> dbScalings = await _context.Scalings.Include(i => i.AlertConfig)
                                                              .Include(x => x.ScalingUsers).ThenInclude(x => x.User)
                                                              .Include(x => x.ScalingGroups).ThenInclude(x => x.Group)
                                                              .Include(x => x.ScalingRoles).ThenInclude(x => x.Role)
                                                              .ToListAsync();

            serviceResponse = dbScalings.Select(c => _mapper.Map<ScalingResponse2>(c)).ToList();
            return serviceResponse;
        }


        public async Task<ScalingResponse2> GetScalingById(int id)
        {
            ScalingResponse2 serviceResponse = new ScalingResponse2();

            ScalingDataModel dbScaling = await _context.Scalings.Include(i => i.AlertConfig)
                                                       .Include(x => x.ScalingUsers).ThenInclude(x => x.User)
                                                       .Include(x => x.ScalingGroups).ThenInclude(x => x.Group)
                                                       .Include(x => x.ScalingRoles).ThenInclude(x => x.Role)
                                                       .FirstOrDefaultAsync(c => c.Id == id);
            serviceResponse = _mapper.Map<ScalingResponse2>(dbScaling);

            return serviceResponse;
        }

        public async Task<List<ScalingResponse2>> GetScalingByAlertConfigId(int id)
        {
            List<ScalingResponse2> serviceResponse = new List<ScalingResponse2>();

            List<ScalingDataModel> dbScaling = await _context.Scalings.Include(i => i.AlertConfig)
                                                                       .Where(i => i.AlertConfig.Id == id)
                                                                       .Include(x => x.ScalingUsers).ThenInclude(x => x.User)
                                                                       .Include(x => x.ScalingGroups).ThenInclude(x => x.Group)
                                                                       .Include(x => x.ScalingRoles).ThenInclude(x => x.Role)
                                                                       .ToListAsync();

            serviceResponse = dbScaling.Select(c => _mapper.Map<ScalingResponse2>(c)).ToList();

            return serviceResponse;
        }


        public async Task<ScalingResponse2> UpdateScaling(UpdateScalingQuery updatedScaling)
        {
            ScalingResponse2 serviceResponse = new ScalingResponse2();
            try
            {
                ScalingDataModel scaling = await _context.Scalings.FirstOrDefaultAsync(c => c.Id == updatedScaling.Id);
                scaling.Nom = updatedScaling.Nom;
                scaling.Conditions = updatedScaling.Conditions;


                _context.Scalings.Update(scaling);
                await _context.SaveChangesAsync();

                serviceResponse = _mapper.Map<ScalingResponse2>(scaling);
            }
            catch (Exception ex)
            {

            }
            return serviceResponse;
        }

        public async Task<List<ScalingResponse2>> DeleteScaling(int id)
        {
            List<ScalingResponse2> serviceResponse = new List<ScalingResponse2>();
            try
            {
                ScalingDataModel scaling = await _context.Scalings.FirstAsync(c => c.Id == id);
                _context.Scalings.Remove(scaling);
                await _context.SaveChangesAsync();

                serviceResponse = (_context.Scalings.Include(i => i.AlertConfig)
                                                         .Include(x => x.ScalingUsers).ThenInclude(x => x.User)
                                                         .Include(x => x.ScalingGroups).ThenInclude(x => x.Group)
                                                         .Include(x => x.ScalingRoles).ThenInclude(x => x.Role)
                                                         .Select(c => _mapper.Map<ScalingResponse2>(c))).ToList();


            }
            catch (Exception ex)
            {

            }
            return serviceResponse;
        }


        public async Task<List<UserResponse>> AddNotification(int id)
        {
            List<UserResponse> serviceResponse = new List<UserResponse>();

            List<ScalingDataModel> scalingsByAlertConfigId = await _context.Scalings.Include(i => i.AlertConfig)
                                                                       .Where(i => i.AlertConfig.Id == id)
                                                                       .Include(x => x.ScalingUsers).ThenInclude(x => x.User)
                                                                       .Include(x => x.ScalingGroups).ThenInclude(x => x.Group)
                                                                       .Include(x => x.ScalingRoles).ThenInclude(x => x.Role)
                                                                       .ToListAsync();
            
            var commande = _context.Commandes
                    .Where(c => c.Quantite > 50)
                    .FirstOrDefault();

            if (commande != null)
            {
                ScalingDataModel scalingIncludingUsers = scalingsByAlertConfigId.First();

                // get all Users belonging to scaling
                List<UserDataModel> scalingUsers = scalingIncludingUsers.ScalingUsers.Select(row => row.User).ToList();

                foreach (var user in scalingUsers)
                {
                    AlertNotificationDataModel newAlertNotification = new AlertNotificationDataModel
                    {
                        Titre = scalingIncludingUsers.AlertConfig.Designation,
                        Description = scalingIncludingUsers.AlertConfig.TextAlert,
                        AlertConfigId = scalingIncludingUsers.AlertConfig.Id,
                        DateEnvoi = DateTime.Now
                    };

                    AlertNotificationUserDataModel alertNotificationUser = new AlertNotificationUserDataModel
                    {
                        AlertNotification = newAlertNotification,
                        User = user
                    };

                    await _context.AlertNotifications.AddAsync(newAlertNotification);
                    await _context.AlertNotificationUsers.AddAsync(alertNotificationUser);

                    await _context.SaveChangesAsync();
                }

                serviceResponse = scalingUsers.Select(c => _mapper.Map<UserResponse>(c)).ToList();
            }
            return serviceResponse;
        }
        /*public async Task<List<UserResponse>> AddNotif(int id)
        {
            List<UserResponse> serviceResponse = new List<UserResponse>();
            var commande = _context.Commandes
                    .Where(c => c.Quantite > 50)
                    .FirstOrDefault();
            if (commande != null)
            {
                ScalingDataModel scalingIncludingUsers = await _context.Scalings.Include(i => i.AlertConfig)
                                                   .Include(x => x.ScalingUsers).ThenInclude(x => x.User)
                                                   .FirstOrDefaultAsync(c => c.Id == id);
                // get all Users belonging to scaling
                List<UserDataModel> scalingUsers = scalingIncludingUsers.ScalingUsers.Select(row => row.User).ToList();

                foreach (var user in scalingUsers)
                {
                    AlertNotificationDataModel newAlertNotification = new AlertNotificationDataModel
                    {
                        Titre = scalingIncludingUsers.AlertConfig.Designation,
                        Description = scalingIncludingUsers.AlertConfig.TextAlert,
                        AlertConfigId = scalingIncludingUsers.AlertConfig.Id,
                        DateEnvoi = DateTime.Now
                    };

                    AlertNotificationUserDataModel alertNotificationUser = new AlertNotificationUserDataModel
                    {
                        AlertNotification = newAlertNotification,
                        User = user
                    };

                    await _context.AlertNotifications.AddAsync(newAlertNotification);
                    await _context.AlertNotificationUsers.AddAsync(alertNotificationUser);

                    await _context.SaveChangesAsync();
                }

                serviceResponse = scalingUsers.Select(c => _mapper.Map<UserResponse>(c)).ToList();
            }
            return serviceResponse;
        }*/

        public async Task<List<UserResponse>> GetUsersByScalingId(int id)
        {
            List<UserResponse> serviceResponse = new List<UserResponse>();

            // get specific scaling including all items
            ScalingDataModel scalingIncludingUsers = await _context.Scalings.Include(i => i.AlertConfig)
                                                   .Include(x => x.ScalingUsers).ThenInclude(x => x.User)
                                                   .FirstOrDefaultAsync(c => c.Id == id);

            // get all Users belonging to scaling
            List<UserDataModel> scalingUsers = scalingIncludingUsers.ScalingUsers.Select(row => row.User).ToList();



            serviceResponse = scalingUsers.Select(c => _mapper.Map<UserResponse>(c)).ToList();
            return serviceResponse;


        }


    }
}

