﻿using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.User.Model;

namespace Web.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;

        public UserService(IMapper mapper, DatabaseContext context)
        {
            _context = context;
            _mapper = mapper;
        }



        public async Task<List<UserResponse>> GetAllUsers()
        {
            List<UserResponse> serviceResponse = new List<UserResponse>();

            //Get the Characters from the database
            List<UserDataModel> dbScalings = await _context.Users.ToListAsync();

            serviceResponse = dbScalings.Select(c => _mapper.Map<UserResponse>(c)).ToList();
            return serviceResponse;
        }


        public async Task<UserResponse> GetUserById(int id)
        {
            UserResponse serviceResponse = new UserResponse();

            UserDataModel dbScaling = await _context.Users.FindAsync(id);
            serviceResponse = _mapper.Map<UserResponse>(dbScaling);

            return serviceResponse;
        }
    }
}
