﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.AlertConfig.Model;

namespace Web.Services.AlertConfigService
{
    public interface IAlertConfigService
    {
        Task<List<AlertConfigResponse>> GetAllAlertConfigs();
        Task<AlertConfigResponse> GetAlertConfigById(int id);
        Task<List<AlertConfigResponse>> AddAlertConfig(CreateAlertConfigQuery newAlertConfig);
        Task<AlertConfigResponse> UpdateAlertConfig(UpdateAlertConfigQuery updatedAlertConfig, int i);
        Task<List<AlertConfigResponse>> DeleteAlertConfig(int id);

    }
}
