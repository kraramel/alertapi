﻿using Common.Email;
using System.Threading.Tasks;

namespace Common.Email
{

    public interface IEmailSender
    {
        /*        Task SendEmailAsync(string email, string subject, string message);
         *        
        */
        Task SendEmailAsync(Message message);
    }
}
