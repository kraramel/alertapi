﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data.Models
{
    public class AlertConfigDataModel
    {
        public int Id { get; set; }
        public string Designation { get; set; }
        public string NiveauCriticite { get; set; }
        public string Etat { get; set; } // Une alertNotification a un etat et non pas une alert config
        public string TextAlert { get; set; }
        public string Module { get; set; }
        public string SourceDonnee { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }
        public List<AlertNotificationDataModel> AlertNotifications { get; set; }
        public List<ScalingDataModel> Scalings { get; set; }
       
        //Frequency of job
        public string Frequence { get; set; }
        //Hour of execute of job
        public string HourOfExecuute { get; set; }

        //Possibility of choosing a day the execute a job
        public DateTime DateOfExecution { get; set; }

        //Date of Expiration of the batch
        public DateTime DateOfExpiration { get; set; }

        //if the frequency is every n minute
        public int ExecuteEveryMinutes { get; set; }

        //if the frequency is every n hour
        public int ExecuteEveryHours { get; set; }
        public int EveryHoursAtMinutes { get; set; }

        //if the frequency is every day or every week
        public Boolean SchedulMonday { get; set; }
        public Boolean SchedulTuesday { get; set; }
        public Boolean SchedulWednesday { get; set; }
        public Boolean SchedulThursday { get; set; }
        public Boolean SchedulFriday { get; set; }
        public Boolean SchedulSaturday { get; set; }
        public Boolean SchedulSunday { get; set; }

        //if the frequency is every month
        public int DayOfMonth { get; set; }
    }
}
