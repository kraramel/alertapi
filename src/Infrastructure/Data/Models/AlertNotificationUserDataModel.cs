﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data.Models
{
    public class AlertNotificationUserDataModel
    {

        public int AlertNotificationId { get; set; }
        public AlertNotificationDataModel AlertNotification { get; set; }
        public int UserId { get; set; }
        public UserDataModel User { get; set; }
    }
}
