﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data.Models 
{
    public class AlertNotificationDataModel
    {
        public int Id { get; set; }
        public int AlertConfigId{ get; set; }
        public string Titre { get; set; }
        public string Description { get; set; }
        public DateTime DateConsultation { get; set; }
        public DateTime DateEnvoi { get; set; }
        public List<AlertNotificationUserDataModel> AlertNotificationUsers { get; set; }
        public AlertConfigDataModel AlertConfig { get; set; }
    }
}
