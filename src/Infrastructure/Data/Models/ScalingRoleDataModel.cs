﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data.Models
{
    public class ScalingRoleDataModel
    {
        public int ScalingId { get; set; }
        public ScalingDataModel Scaling { get; set; }
        public int RoleId { get; set; }
        public RoleDataModel Role { get; set; }
    }
}
