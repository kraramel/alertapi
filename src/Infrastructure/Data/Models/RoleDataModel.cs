﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Infrastructure.Data.Models
{
    public class RoleDataModel 
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public DateTime CreationDate { get; set; }
        public int DomainId { get; set; }
        [JsonIgnore]
        public DomainDataModel Domain { get; set; }
/*        public IList<RoleGroup> RoleGroup { get; set; }
*/

        public List<ScalingRoleDataModel> ScalingRoles { get; set; }
    }
}
