﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data.Models
{
    public class ScalingDataModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Conditions { get; set; }
        public AlertConfigDataModel AlertConfig { get; set; }
        public List<ScalingUserDataModel> ScalingUsers { get; set; }
        public List<ScalingGroupDataModel> ScalingGroups { get; set; }
        public List<ScalingRoleDataModel> ScalingRoles { get; set; }

    }
}
