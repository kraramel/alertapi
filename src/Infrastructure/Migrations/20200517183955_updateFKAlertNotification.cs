﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class updateFKAlertNotification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AlertNotifications_AlertConfigs_AlertConfigId",
                table: "AlertNotifications");

            migrationBuilder.AlterColumn<int>(
                name: "AlertConfigId",
                table: "AlertNotifications",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AlertNotifications_AlertConfigs_AlertConfigId",
                table: "AlertNotifications",
                column: "AlertConfigId",
                principalTable: "AlertConfigs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AlertNotifications_AlertConfigs_AlertConfigId",
                table: "AlertNotifications");

            migrationBuilder.AlterColumn<int>(
                name: "AlertConfigId",
                table: "AlertNotifications",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_AlertNotifications_AlertConfigs_AlertConfigId",
                table: "AlertNotifications",
                column: "AlertConfigId",
                principalTable: "AlertConfigs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
